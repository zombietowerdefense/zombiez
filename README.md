Zombie Tower Defense
====================

BRAAAAAAAAIIIIIINNNNNSS
-----------------------

The "smart" way to start your day!

![hehehe](http://i.imgur.com/L0BdHiK.png)

Configuring the Build Path
--------------------------

Adapted from the [NinjaCava wiki page](http://slick.ninjacave.com/wiki/index.php?title=Setting_up_Slick2D_with_Eclipse).

### Set up Slick2D and LWJGL

Extracted from [LWJGL with Eclipse](http://wiki.lwjgl.org/index.php?title=Setting_Up_LWJGL_with_Eclipse)

1. Open up Eclipse.
2. Go to Project --> Properties in the menu bar.
3. Click on Java Build Path.
4. Click Add Library...
5. Select User Library
6. Click Next
7. Click User Libraries
8. In the user libraries dialog select New
9. Type in Slick2D or any other name that you want for the Library Name, click ok
10. Select the new library and click the Add Jar button.
11. Go to 'slick2d > lib' and add *all the JARs*.

### Natives

1. Right-Click your project node and click Build Path > Configure Build Path
2. Go to the Libraries tab
3. Expand the Slick2D Library
4. Click the Natives Library Location and click the Edit button
5. Click Workspace...
5. Navigate to 'slick2d > lib' and select the sub folder for your OS
6. Click OK

That's it.
