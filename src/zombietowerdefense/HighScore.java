package zombietowerdefense;

public class HighScore implements Comparable<HighScore> {

	private String playerName;
	private int score;

	public HighScore(String playerName, int score) {
		this.playerName = playerName;
		this.score = score;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int compareTo(HighScore otherHighScore) {
		if (otherHighScore.getScore() > 0) {
			return -1;
		} else if (otherHighScore.getScore() < 0) {
			return 1;
		} else {
			return 0;
		}
	}
}
