package zombietowerdefense;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;

public class DeadZombie extends Animation {
	private float posX;
	private float posY;

	public DeadZombie(SpriteSheet frames, int x1, int y1, int x2, int y2, boolean horizontalScan, int duration,
			boolean autoUpdate, float x, float y) {

		super(frames, x1, y1, x2, y2, horizontalScan, duration, autoUpdate);
		posX = x;
		posY = y;
		stopAt(5);
	}

	public void draw() {
		draw(posX - 64, posY - 64);
	}
}
