package zombietowerdefense;

/**
 * Syntactic sugar for making a new gun with the properties of a revolver.
 * 
 * @author chris
 *
 */
public class Revolver extends Gun {

	public Revolver() {
		super("Revolver", 6, 6, "assets/rev-wav", "assets/revolver.png");
		super.setDamage(50);
	}
}
