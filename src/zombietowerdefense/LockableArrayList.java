package zombietowerdefense;

import java.util.ArrayList;

@SuppressWarnings("serial")
public class LockableArrayList<E> extends ArrayList<E> {

	private boolean locked;
	
	public LockableArrayList() {
		super();
		locked = false;
	}
	
	public boolean isLocked() {
		return locked;
	}
	
	public void lock() {
		while (locked) {
		}
		locked = true;
	}
	
	public void unlock() {
		locked = false;
	}
}
