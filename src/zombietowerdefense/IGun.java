package zombietowerdefense;

public interface IGun {

	public void fire();
	
	public String getName();
	
	public void setRounds(int rounds);
	public int getRounds();
	
	public void setCooldown(double cool);
	public double getCooldown();
	
	public void setReloadCooldown(double cool);
	public double getReloadCooldown();
	
	public void setDamage(double damage);
	public double getDamage();
	
}
