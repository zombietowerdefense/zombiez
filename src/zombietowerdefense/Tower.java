package zombietowerdefense;

import java.util.Set;

import org.newdawn.slick.geom.Circle;

public class Tower extends Circle {

	private float damage;
	private int fireDelay;
	private int deltaCounter;

	public Tower(float centerPointX, float centerPointY, float radius, float damage, int fireDelay) {
		super(centerPointX, centerPointY, radius);
		this.damage = damage;
		this.fireDelay = fireDelay;
	}

	/**
	 * returns reference to the zombie shot.
	 * 
	 * @param keys
	 * @param delta
	 * @return
	 */
	public Zombie tryToShoot(Set<Integer> keys, int delta) {
		Zombie mZombie = null;
		float max = 0;
		deltaCounter += delta;
		if (deltaCounter < fireDelay)
			return null;
		deltaCounter -= fireDelay;

		for (Integer i : keys) {
			IZombie zombie = Main.zombies.get(i);
			if (((Zombie) zombie).getCenterX() > max) {
				if (!zombie.isDead()) {
					max = ((Zombie) zombie).getCenterX();
					mZombie = (Zombie) zombie;
				}
			}
		}

		if (mZombie != null) {
			mZombie.hit(damage);
			mZombie.stumble(true);
		} else
			deltaCounter = fireDelay;

		return mZombie;
	}

}
