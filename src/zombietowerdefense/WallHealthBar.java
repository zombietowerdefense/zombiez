package zombietowerdefense;

import org.newdawn.slick.Color;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

public class WallHealthBar extends Rectangle {

	private ShapeFill fill;
	private Wall wall;

	public WallHealthBar(Wall wall) {
		super(0, 0, 0, 0);

		this.wall = wall;

		setHeight(wall.getHeight());
		setWidth(10);
		setX(wall.getX() + wall.getWidth() - this.getWidth());
		setY(wall.getY());

		fill = new GradientFill(0, 0, Color.blue, 100, 100, Color.red);
	}

	public ShapeFill getFill() {
		update(); // dirty trick.

		return fill;
	}

	public void update() {
		setHeight((float) (wall.getHeight() * (wall.getHealthPercentage() / 100)));
	}
}
