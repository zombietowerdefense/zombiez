package zombietowerdefense;

import java.util.ArrayList;
import java.util.Set;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Field extends BasicGameState {

	private Smeed smeed;
	private Image kana;

	private static GameTime gameTime;
	private float smeedX;
	private float smeedY;

	private boolean clicked;
	private float mouseX;
	private float mouseY;
	private Input input;
	public static SpriteSheet sprites;
	private Wall wall;
	private ArrayList<DeadZombie> dedZed;

	private WallHealthBar wallHealthBar;

	public static int money = 0;
	private ArrayList<Tower> towers;
	private Rectangle buyTowerButton;
	private float towerYPos = 200;
	private float towerXPos;
	private int numTowers;
	private int towerCost;

	private double gunAngle;
	private final float smeedGunScale = 0.2f;

	GameContainer globalContainer;
	private long lastKeyPressTime;
	private final int KEY_PRESS_DELAY = 1000;

	public void init(GameContainer container, StateBasedGame game) throws SlickException {

		lastKeyPressTime = System.currentTimeMillis() - KEY_PRESS_DELAY;

		globalContainer = container;

		input = new Input(container.getHeight());

		smeed = new Smeed();
		kana = new Image("assets/kanaoffice-incl.png");
		smeedX = container.getWidth() - 250;
		smeedY = (container.getHeight() / 10) * 6;

		sprites = new SpriteSheet("assets/zombie_0.png", 128, 128);

		wall = new Wall((container.getWidth() / 4) * 3, 0, 30, container.getHeight());
		dedZed = new ArrayList<DeadZombie>();
		towers = new ArrayList<Tower>();
		towerXPos = wall.getCenterX() + 50;

		wallHealthBar = new WallHealthBar(wall);

		buyTowerButton = new Rectangle(container.getWidth() - 200, 30, 150, 30);

		gameTime = new GameTime();

		money = 0;
		towerCost = 100;
		numTowers = 0;
		clicked = false;

	}

	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {

		funWithGameTime(container);

		Main.zombies = Main.zombieFactory.getZombies();

		input.poll(container.getWidth(), container.getHeight());
		mouseX = input.getMouseX();
		mouseY = input.getMouseY();

		gunAngle = getAngle(smeedX, smeedY, mouseX, mouseY);
		Image smeedGun = smeed.getGun().getImage();
		smeedGun.setCenterOfRotation(smeedGun.getWidth() / 2 * smeedGunScale, smeedGun.getHeight() / 2 * smeedGunScale);
		smeedGun.setRotation((float) gunAngle);

		if (clicked) {
			if (buyTowerButton.contains(mouseX, mouseY)) {

				addTower(container, false);

			} else {
				smeed.shoot();
			}
		}

		for (Tower tower : towers)
			tower.tryToShoot(Main.zombies.keySet(), delta);

		Set<Integer> keys = Main.zombies.keySet();
		for (Integer i : keys) {
			IZombie zombie = Main.zombies.get(i);
			zombiesDoStuff((Zombie) zombie, delta, game);
			if (clicked && ((Zombie) zombie).contains(mouseX, mouseY)) {
				zombie.hit(smeed.getGun().getDamage());
				((Zombie) zombie).stumble(true);
			}

			if (zombie.isDead()) {
				money = money + 100;
				dedZed.add(new DeadZombie(sprites, 22, 4, 28, 4, false, 100, true, ((Zombie) zombie).getCenterX(),
						((Zombie) zombie).getCenterY()));
				Main.zombies.remove(i);

			}

		}

		wall.heal(1.5);

		clicked = false;

		if (input.isKeyDown(Input.KEY_T) && System.currentTimeMillis() >= lastKeyPressTime + KEY_PRESS_DELAY) {
			addTower(container, false);
			Keyboard.destroy();
			try {
				Keyboard.create();
			} catch (LWJGLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		// draw background
		Image background = new Image("assets/bg.png");
		background.draw(0, 0, container.getWidth(), container.getHeight());

		g.setColor(Color.white);
		g.drawString("Money: " + money, 400, 50);
		g.fill(buyTowerButton);

		g.setColor(Color.black);
		g.drawString("Buy tower:" + towerCost, buyTowerButton.getCenterX() - 60, buyTowerButton.getCenterY() - 7);

		// draw dead zombies
		for (DeadZombie anim : dedZed)
			anim.draw();
		// draw zombies
		Set<Integer> keys = Main.zombies.keySet();
		for (Integer i : keys) {
			IZombie zombie = Main.zombies.get(i);

			if (((Zombie) zombie).getAnim() == null) {
				((Zombie) zombie).setAnim(sprites, 4, 4, 8, 4, 200);
			}

			((Zombie) zombie).getAnim().draw(((Zombie) zombie).getCenterX() - 64, ((Zombie) zombie).getCenterY() - 64);
		}

		kana.draw(container.getWidth() - 250, (container.getHeight() / 4), 1.5f);

		// draw wall
		g.fill(wall);
		for (Tower tower : towers)
			g.fill(tower);

		// draw wall health bar
		g.fill(wallHealthBar, wallHealthBar.getFill());

		// Draw Smeed last cause he's "on top of everything" ... #SmeedFirst
		smeed.getImage().draw(container.getWidth() - 200, (int) ((container.getHeight() / 4) * 1.5), 0.6f);
		smeed.getGun().getImage().draw(smeedX + 20, smeedY, smeedGunScale);
	}

	private void zombiesDoStuff(Zombie zombie, int delta, StateBasedGame game) {
		zombie.update(delta);
		if (zombie.getCenterX() + 10 >= wall.getCenterX() - 20) {
			zombie.setCenterX(wall.getCenterX() - 30);
			if (zombie.tryToHitWall(delta))
				wall.hit(zombie.getDamage());
			if (wall.isDead()) {
				gameOver(game);
			}
		}

		if (zombie.getAnim() == null)
			zombie.setAnim(sprites, 4, 4, 8, 4, 200);
	}

	private boolean addTower(GameContainer container, boolean free) {
		if (numTowers < 45) {

			if (!free) { // must we pay?
				if (money >= towerCost) { // can we afford it?
					money = money - towerCost; // pay up.
					towerCost += towerCost / 2;
				} else {
					return false;
				}
			}

			towers.add(new Tower(towerXPos, towerYPos, 10, 100, 5000));
			towerYPos += 40;
			if (towerYPos > container.getHeight() - 50) {
				towerYPos = 200;
				towerXPos += 40;
			}

			numTowers++;

			return true;
		}

		return false;
	}

	private double getAngle(float x1, float y1, float x2, float y2) {
		double theta = Math.atan2(y2 - y1, x2 - x1);
		theta += Math.PI / 2.0;
		double angle = Math.toDegrees(theta) + 90;

		if (angle < 0) {
			angle += 360;
		}

		return angle;
	}

	public void mouseClicked(int button, int x, int y, int clickCount) {
		clicked = true;
	}

	public int getID() {
		return 1;
	}

	private void funWithGameTime(GameContainer container) {
		long secondsElapsed = gameTime.secondsElapsed();

		if (wall.getHealthPercentage() < 50) {
			smeed.turnRed();

			// Yes this is very bad ...
		} else if (Main.zombies != null && Main.zombies.size() > 15) {
			for (IZombie zombie : Main.zombies.values()) {
				if (((Zombie) zombie).getCenterX() > (container.getWidth() - (container.getWidth() - wall.getX())) / 2) {
					smeed.turnRedder();
					break;
				}
			}
		} else {
			smeed.turnBlack();
		}

		Main.zombieFactory.setDifficulty(secondsElapsed);
	}

	private void gameOver(StateBasedGame game) {
		GameOver.resetTime();
		game.enterState(2);
	}

	public static void resetTime() {
		gameTime.reset();
	}

	public void cheat(ArrayList<String> cheats) {
		if (cheats.contains("ireallysuck")) { // enable 20 free towers.
			for (int i = 0; i < 20; i++) {
				addTower(globalContainer, true);
			}
		}
	}

}
