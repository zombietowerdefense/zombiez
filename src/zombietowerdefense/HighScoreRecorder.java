package zombietowerdefense;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class HighScoreRecorder {

	private final String highScoreFilepath = "assets/highscores.txt";
	private ArrayList<HighScore> highScores;

	public HighScoreRecorder() {
		highScores = readScores();
	}

	public ArrayList<HighScore> getScores() {
		return highScores;
	}

	public void addScore(String playerName, int score) {
		HighScore newHighScore = new HighScore(playerName, score);
		highScores.add(newHighScore);

		Collections.sort(highScores);

		highScores.remove(3);

		writeScores();
	}

	public boolean isHighScore(int potentialScore) {
		for (HighScore highScore : highScores) {
			if (potentialScore > highScore.getScore()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Read scores from file.
	 * 
	 * @return an ArrayList of HighScores.
	 */
	private ArrayList<HighScore> readScores() {
		ArrayList<HighScore> highScores = new ArrayList<HighScore>();

		try (FileReader fr = new FileReader(highScoreFilepath);
				BufferedReader br = new BufferedReader(fr);
				Scanner s = new Scanner(br);) {

			while (s.hasNext()) {
				String playerName = s.next();
				int score = s.nextInt();

				HighScore highScore = new HighScore(playerName, score);
				highScores.add(highScore);
			}

		} catch (FileNotFoundException e) {
			// Quietly create the file.
			writeScores();
		} catch (IOException e) {
			System.err.println("IO Error involving high scores file.");
			e.printStackTrace();
		}

		if (highScores.size() != 3) {
			System.err.println("There are " + highScores.size() + " high scores.");
		}

		return highScores;
	}

	/**
	 * Write scores to file.
	 * 
	 */
	private void writeScores() {
		try (FileWriter fw = new FileWriter(highScoreFilepath); BufferedWriter bw = new BufferedWriter(fw);) {

			if (highScores != null) {
				for (HighScore score : highScores) {
					bw.write(score.getPlayerName() + " " + score.getScore() + "\n");
				}

			} else {
				System.err.println("highScores is null. wtf");
			}

		} catch (IOException e) {
			System.err.println("IO Error involving high scores file.");
			e.printStackTrace();
		}
	}
}
