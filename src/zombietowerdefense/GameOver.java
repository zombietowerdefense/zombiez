package zombietowerdefense;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GameOver extends BasicGameState {

	private boolean clicked;
	private static GameTime gameTime;
	private HighScoreRecorder highScoreRecorder;

	public GameOver() {
		clicked = false;
		gameTime = new GameTime();
		// highScoreRecorder = new HighScoreRecorder();
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {

	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {

		g.setColor(Color.white);
		g.drawString("DISADVANTAGED!: " + Field.money,
				container.getWidth() / 3, container.getHeight() / 3);

		//displayHighScores(container,game,g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {

		if (clicked && gameTime.secondsElapsed() > 2) {
			game.getState(0).init(container, game);
			game.getState(1).init(container, game);
			game.getState(2).init(container, game);
			game.enterState(0);
		}

		clicked = false;
	}

	public void mouseClicked(int button, int x, int y, int clickCount) {
		clicked = true;
	}

	@Override
	public int getID() {
		return 2;
	}

	public static void resetTime() {
		gameTime.reset();
	}

	private void displayHighScores(GameContainer container,
			StateBasedGame game, Graphics g) {
		ArrayList<HighScore> highScores = highScoreRecorder.getScores();

		g.drawString("High Scores", container.getWidth() / 2,
				container.getHeight() / 8);

		for (int i = 0; i < highScores.size(); i++) {
			HighScore score = highScores.get(i);

			g.drawString(score.getPlayerName(), container.getWidth() / 2,
					container.getHeight() / (8 - (i + 2)));
		}
	}
}
