package zombietowerdefense;

public class GameTime {

	private long startTime;

	public GameTime() {
		startTime = System.currentTimeMillis() / 1000;
	}

	public long secondsElapsed() {
		return System.currentTimeMillis() / 1000 - startTime;
	}

	public void reset() {
		startTime = System.currentTimeMillis() / 1000;

	}
}
