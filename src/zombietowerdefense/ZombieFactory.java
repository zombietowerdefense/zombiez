package zombietowerdefense;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

public class ZombieFactory {

	private static Random random = new Random();
	private static Timer timer = new Timer();
	// private static LockableArrayList<IZombie> zombies = new
	// LockableArrayList<IZombie>();

	private static ConcurrentHashMap<Integer, IZombie> zombies = new ConcurrentHashMap<Integer, IZombie>();
	private static Integer numZombies = 0;

	private static boolean isRunning = false;
	private static double lambda = 6.0;

	private float levelMod = 1;

	class Task extends TimerTask {
		@Override
		public void run() {
			List<ZombieType> zombieTypes = Collections.unmodifiableList(Arrays.asList(ZombieType.values()));
			int rndZombieType = randomNumber(0, zombieTypes.size());
			ZombieType type = zombieTypes.get(rndZombieType);

			double randomWait = levelMod * (-Math.log(1.0 - random.nextDouble()) / lambda) * 1.5;
			long rndWait = (long) randomWait;
			timer.schedule(new Task(), rndWait);

			IZombie zombie = null;

			double h = randomNumber(30, 100);
			double v = randomNumber(70, 80);
			double d = randomNumber(30, 100);

			int randx = -30; // randomNumber(100,150);
			int randy = randomNumber(Main.game.getHeight() / 3, Main.game.getHeight() - 10);

			zombie = new Zombie(type, h, v, d, randx, randy);

			// zombies.lock();
			zombies.put(numZombies, zombie);
			numZombies++;
			System.out.format("[ZF] Zombie added. %s - time: %dms\n", zombie.toString(), rndWait);
			// zombies.unlock();
		}
	}

	public ZombieFactory() {
		System.out.format("%s\n", "Zombie Factory initialized");
	}

	public ConcurrentHashMap<Integer, IZombie> getZombies() {
		return zombies;
	}

	public void update() {
		int speed = 1;

		if (speed == 0) {
			start();
		} else if (!isRunning) {
			start();
		}
		setLambda(speed / 1000.0);
	}

	public void start() {
		clearZombies();
		isRunning = true;
		update();
		create();
	}

	public void create() {
		if (isRunning) {
			new Task().run();
		}
	}

	public void stop() {
		timer.cancel();
		timer = new Timer();
		clearZombies();
		isRunning = false;
	}

	public void clearZombies() {
		zombies.clear();
	}

	public int numZombies() {
		return zombies.size();
	}

	public void setLambda(double lam) {
		lambda = lam;
	}

	public static int randomNumber(int min, int max) {
		int rnd = random.nextInt(max - min) + min;
		return rnd;
	}

	public static void update(int ms) {
		Set<Integer> keys = zombies.keySet();
		for (Integer i : keys) {
			IZombie zombie = zombies.get(i);
			zombie.update(ms);

			if (zombie.isDead()) {
				zombies.remove(i);
			}
		}
	}

	public void setDifficulty(long secondsElapsed) {
		// The zombies come in waves ...
		levelMod = (float) Math.sin(secondsElapsed);

		if (levelMod < 0) {
			levelMod *= -1;
		}

		if (secondsElapsed > 20 && levelMod > 0.5) {
			if (secondsElapsed < 60) {
				levelMod -= 0.2;
			}
			levelMod = 1 - levelMod;
		}

		float minLevelMod = 0.3f;
		if (secondsElapsed >= 45 && secondsElapsed < 120) { // played for 45
															// seconds yet?
			minLevelMod = 0.25f; // turn up the heat ;)
		} else if (secondsElapsed >= 120) {// played for 2 minutes yet?
			minLevelMod = 0.1f; // Turn up the heat even more.
		}

		if (levelMod < minLevelMod) {
			levelMod = minLevelMod;
		}
	}
}
