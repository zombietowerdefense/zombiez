package zombietowerdefense;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Player {

	private final int MAX_HEALTH = 100;
	private final int MIN_HEALTH = 100;

	private int health;
	private Gun gun;

	private Image image;

	public Player(String imagePath) {
		setHealth(MAX_HEALTH);
		gun = new Revolver();

		try {
			image = new Image(imagePath);
		} catch (SlickException e) {
			System.out.println("It's worse than that he's dead Jim, dead Jim, dead Jim, dead!");
			e.printStackTrace();
		}
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int newHealth) {
		if (newHealth > MAX_HEALTH)
			health = MAX_HEALTH;
		else if (newHealth < MIN_HEALTH)
			health = MIN_HEALTH;
		else
			health = newHealth;
	}

	public void shoot() {
		gun.fire();
	}

	public Image getImage() {
		return image;
	}

	public Gun getGun() {
		return gun;
	}

	protected void setImage(Image newImage) {
		image = newImage;
	}
}
