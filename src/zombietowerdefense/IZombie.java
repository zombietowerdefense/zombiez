package zombietowerdefense;

public interface IZombie {
	
	public ZombieType getType();
	
	public void setVelocity(double velocity);
	public double getVelocity();
	
	public void setHealth(double health);
	public double getHealth();
	
	public void setDamage(double toDeal);
	public double getDamage();
	
	public void update(double delta);
	//public float getXPosition();
	//public float getYPosition();
	
	public boolean isDead();
	public void hit(double dmg);
	
}
