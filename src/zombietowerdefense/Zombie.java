package zombietowerdefense;

import java.util.Random;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Music;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

public class Zombie extends Rectangle implements IZombie {

	private static final long serialVersionUID = 1L;
	private double health;
	private double damage;
	private double velocity;
	private ZombieType type;
	private boolean dead;
	private Animation anim = null;

	private int hitWall = 0;
	private boolean stumbling = false;
	private Animation oldAnim;

	public Zombie(ZombieType type, double health, double velocity, double damage, int x, int y) {
		super(x, y, 30, 40);

		this.type = type;
		setHealth(health);
		setVelocity(velocity);
		setDamage(damage);
	}

	@Override
	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}

	@Override
	public double getVelocity() {
		return velocity;
	}

	@Override
	public void setHealth(double health) {
		this.health = health;
	}

	@Override
	public double getHealth() {
		return health;
	}

	@Override
	public void setDamage(double toDeal) {
		this.damage = toDeal;
	}

	@Override
	public double getDamage() {
		return damage;
	}

	@Override
	public String toString() {
		return String.format("%s Zombie: %f health, %f velocity, deals %f damage", type.name(), health, velocity,
				damage);
	}

	@Override
	public ZombieType getType() {
		return type;
	}

	@Override
	public void update(double delta) {
		int d = (int) (velocity * (delta / 1000));
		setCenterX(getCenterX() + d);

		if (stumbling && anim.isStopped()) {
			anim = oldAnim;
			stumbling = false;
		}
	}

	@Override
	public void hit(double damage) {
		setHealth(getHealth() - damage);
		if (getHealth() <= 0)
			dead = true;

		final String soundEffectPath = Main.zombieSounds.get(new Random().nextInt(Main.zombieSounds.size()));

		try {
			Music music = new Music(soundEffectPath);
			music.play();

			// Music fireSoundEffect = new Music(soundEffectPath);
			// fireSoundEffect.play(1.0f, 1.0f);
			System.out.println("Gun fired!");
		} catch (Exception e) {
			System.err.println("Can't give gun sound effect with file " + soundEffectPath);
			e.printStackTrace();
		}
	}

	public boolean isDead() {
		return dead;
	}

	public boolean tryToHitWall(int delta) {
		hitWall += delta;
		if (hitWall >= 1000) {
			hitWall -= 1000;
			return true;
		}
		return false;
	}

	public void setAnim(SpriteSheet sprites, int x1, int y1, int x2, int y2, int speed) {
		anim = new Animation(sprites, x1, y1, x2, y2, false, speed, true);
		anim.setLooping(true);
	}

	public Animation getAnim() {
		return anim;
	}

	public void stumble(boolean tf) {
		oldAnim = anim;
		setAnim(Field.sprites, 14, 4, 19, 4, 20);
		anim.setLooping(false);
		stumbling = true;
	}

}
