package zombietowerdefense;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.TrueTypeFont;

public class Main {

	public static AppGameContainer game;

	public static TrueTypeFont font;
	public static TrueTypeFont flashFont;
	public static Font fontBase;
	public static String flash;
	public static Font fontFlash;

	public static ConcurrentHashMap<Integer, IZombie> zombies;
	public static ZombieFactory zombieFactory = new ZombieFactory();

	public static ArrayList<String> zombieSounds;

	public static void loadResources() throws FontFormatException, IOException {
		Font fontRaw = Font.createFont(Font.TRUETYPE_FONT, new File("assets/scary.ttf"));

		fontFlash = fontRaw.deriveFont(Font.PLAIN, 16f);
		flashFont = new TrueTypeFont(fontFlash, true);
		fontBase = fontRaw.deriveFont(Font.BOLD, 24f);
		font = new TrueTypeFont(fontBase, true);

		zombieSounds = new ArrayList<String>();
		File folder = new File("assets/zombie");

		for (File file : folder.listFiles()) {
			if (file.isFile()) {
				zombieSounds.add(file.getAbsolutePath());
			}
		}
	}

	public static void main(String[] args) throws SlickException {

		Game g = new Game("Zombie Tower Defense");
		game = new AppGameContainer(g);
		AppGameContainer game = new AppGameContainer(g);
		BGMusic bg = new BGMusic();
		bg.run();

		game.setDisplayMode(1024, 600, false);
		game.setVSync(false);
		game.setShowFPS(false);
		game.setAlwaysRender(true);

		game.start();
	}

}

class BGMusic implements Runnable {

	public void run() {
		Sound Sound_1;
		try {
			Sound_1 = new Sound("assets/bg.ogg");
			Sound_1.loop(1.0f, 0.8f);
		} catch (SlickException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		new Thread(new BGMusic()).start();
	}
}
