package zombietowerdefense;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.RoundedRectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Menu extends BasicGameState {

	private GameContainer c;

	private static int buttonWidth = 300;
	private static int buttonHeight = 50;
	private RoundedRectangle startButton;
	private RoundedRectangle musicButton;
	private RoundedRectangle FSButton;
	private Color buttonColor;
	private Color textColor;
	private boolean clicked = false;
	private int mouseX = 0;
	private int mouseY = 0;
	// These aren't being used.
	// private Options options;
	// private Animation phone;
	private Music Sound_1;
	private Image mpic;

	private Input input; // input detector for entering cheat codes.
	private long lastKeyPressTime;
	private final int KEY_PRESS_DELAY = 1000;
	private ArrayList<String> enabledCheats;
	private boolean cheatsEnabled = true;

	public Menu(/* Options o */) {
		// options = o;
		input = new Input(0);
		lastKeyPressTime = System.currentTimeMillis() - KEY_PRESS_DELAY;
		enabledCheats = new ArrayList<String>();
	}

	public void init(GameContainer container, StateBasedGame game) throws SlickException {

		try {
			Main.loadResources();
		} catch (FontFormatException e) {
			System.err.println("Error loading resources.");
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		c = container;
		reiniti(c);
	}

	private void reiniti(GameContainer container) throws SlickException {
		int x1 = (container.getWidth() / 2) - (buttonWidth / 2);

		int yStart = (container.getHeight() / 2) - buttonHeight;
		int yFS = (container.getHeight() / 2) + buttonHeight;

		startButton = new RoundedRectangle(x1, yStart, buttonWidth, buttonHeight, 5);
		musicButton = new RoundedRectangle(x1, 200, buttonWidth, buttonHeight, 5);
		FSButton = new RoundedRectangle(x1, yFS, buttonWidth, buttonHeight, 5);

		buttonColor = new Color(100, 100, 100, 200);
		textColor = new Color(250, 20, 20);

		mpic = new Image("assets/cartoon-zombie-01.png");
	}

	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {

		if (clicked) {
			// if start is clicked change to running state
			if (mouseX < startButton.getMaxX() && mouseX > startButton.getMinX() && mouseY < startButton.getMaxY()
					&& mouseY > startButton.getMinY()) {
				Field.resetTime();

				// This could well break at some point down the line ;)
				((Field) game.getState(1)).cheat(enabledCheats);
				game.enterState(1);

				Main.zombieFactory.start();
			}

			clicked = false;
		}
		if (container.getInput().isKeyPressed(Keyboard.KEY_ESCAPE)) {
			container.setFullscreen(false);
		}

		if (cheatsEnabled) {
			if (input.isKeyDown(Input.KEY_C) && System.currentTimeMillis() >= lastKeyPressTime + KEY_PRESS_DELAY) {
				lastKeyPressTime = System.currentTimeMillis();
				System.out.println("C Pressed! Let's cheat...");

				enabledCheats.add(JOptionPane.showInputDialog("Cheat"));

				// stop key repeating when focus is lost.
				Keyboard.destroy();
				try {
					Keyboard.create();
				} catch (LWJGLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {

		g.setColor(new Color(255, 255, 255));
		Rectangle r = new Rectangle(0, 0, container.getWidth(), container.getHeight());
		g.fill(r);

		g.setAntiAlias(true);
		mpic.draw(0, 0, container.getWidth(), container.getHeight());

		g.scale(1, 1);
		g.setFont(Main.font);
		g.setColor(buttonColor);
		g.fill(startButton);
		// g.fill(musicButton);
		// g.fill(FSButton);

		g.setColor(textColor);
		Canvas c = new Canvas();
		FontMetrics metrics = c.getFontMetrics(Main.fontBase);

		g.drawString("Start", startButton.getCenterX() - metrics.stringWidth("Start") / 2, startButton.getCenterY() - 5);
	}

	public void toggleResolution() throws SlickException {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		Main.game.setDisplayMode(width, height, false);
	}

	public void mouseClicked(int button, int x, int y, int clickCount) {
		mouseX = x;
		mouseY = y;
		clicked = true;
	}

	public int getID() {
		return 0;
	}

	public ArrayList<String> getEnabledCheats() {
		return enabledCheats;
	}

}