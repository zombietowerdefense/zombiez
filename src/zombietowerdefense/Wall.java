package zombietowerdefense;

import org.newdawn.slick.geom.Rectangle;

public class Wall extends Rectangle {

	private final int deadX = 100000;

	private double maxHealth = 10000;
	private double health = maxHealth;

	public Wall(float x, float y, float width, float height) {
		super(x, y, width, height);
	}

	public double getHealth() {
		return health;
	}

	public void heal(double h) {
		health += h;
		if (health > maxHealth)
			health = maxHealth;
	}

	public void hit(double d) {
		health -= d;
		if (health <= 0)
			die();
	}

	public void die() {
		setCenterX(deadX);
	}

	public boolean isDead() {
		return this.getCenterX() == deadX;
	}

	public double getHealthPercentage() {
		return (health / maxHealth) * 100;
	}

}
