package zombietowerdefense;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;

public class Gun implements IGun {

	private final int max;
	private final String name;
	private int capacity;
	private double cooldown;
	private double reloadCooldown;
	private double damage;

	private ArrayList<String> soundEffectPaths;
	private Image image;

	public Gun(String name, int rounds, int max, String soundEffectDirectory, String imagePath) {
		this.max = max;
		this.name = name;
		this.capacity = rounds;

		soundEffectPaths = new ArrayList<String>();

		File folder = new File(soundEffectDirectory);

		for (File file : folder.listFiles()) {
			if (file.isFile()) {
				soundEffectPaths.add(file.getPath());
			}
		}

		try {
			image = new Image(imagePath);
		} catch (SlickException e) {
			System.err.print("I cannae dae it! ");
			System.err.print("Shit hit the fan when attempting to set new gun image ");
			System.err.print("from path \"" + imagePath + "\"\n");
			e.printStackTrace();
		}
	}

	@Override
	public void fire() {
		if (capacity == 1) {
			reload();
		}
		capacity--;

		// TODO: make sure this stuff works.
		final String soundEffectPath = soundEffectPaths.get(new Random().nextInt(soundEffectPaths.size()));

		System.out.println();

		try {
			Music music = new Music(soundEffectPath);
			music.play();

			System.out.println("Gun fired!");
		} catch (Exception e) {
			System.err.println("Can't give gun sound effect with file " + soundEffectPath);
			e.printStackTrace();
		}
	}

	public void reload() {
		try {
			Thread.sleep((long) reloadCooldown);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		capacity = max;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setRounds(int num) {
		this.capacity = num;
	}

	@Override
	public int getRounds() {
		return capacity;
	}

	@Override
	public void setCooldown(double cool) {
		this.cooldown = cool;
	}

	@Override
	public double getCooldown() {
		return cooldown;
	}

	@Override
	public void setReloadCooldown(double cool) {
		this.reloadCooldown = cool;
	}

	@Override
	public double getReloadCooldown() {
		return this.reloadCooldown;
	}

	@Override
	public void setDamage(double damage) {
		this.damage = damage;
	}

	@Override
	public double getDamage() {
		return this.damage;
	}

	public Image getImage() {
		return image;
	}

}
